
//wow animation

new WOW().init();


//Banner Slider

$('#banner_slider').owlCarousel({
  loop: true,
  margin: 0,
  nav: false,
  mouseDrag: true,
  autoplay: true,
  video: true,
  lazyLoad: true,
  autoplayTimeout: 8000,
  animateOut: 'slideOutUp',
  dots: true,
  dotsData: false,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items: 1
    }
  },
});





//Equal Height box
$(document).ready(function () {
  $('.home_sec3').each(function () {
    var highestBox = 0;
    $('.boxs', this).each(function () {
      if ($(this).height() > highestBox) {
        highestBox = $(this).height();
      }
    });
    $('.boxs', this).height(highestBox);
  });
});




